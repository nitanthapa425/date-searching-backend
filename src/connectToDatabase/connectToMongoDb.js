import mongoose from "mongoose";

// let dbUrl = "mongodb://0.0.0.0:27017/dw7";
// let dbUrl = "mongodb+srv://nitanthapa123:nitanthapa123@deerwalk.dvj9q6u.mongodb.net/order"
let dbUrl = `mongodb+srv://nitanthapa:nitanthapa@coder.p5h4bvk.mongodb.net/order`

let connectToMongoDb = async () => {
  try {
    await mongoose.connect(dbUrl);
    console.log(
      `application is connected to mongodb at port ${dbUrl} successfully`
    );
  } catch (error) {
    console.log(error.message);
  }
};

export default connectToMongoDb;
