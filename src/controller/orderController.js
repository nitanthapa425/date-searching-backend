import NepaliDate from "nepali-date-converter";
import { Order } from "../Schema/model.js";

export let createOrder = async (req, res) => {
  let data = req.body;
  try {
    let result = await Order.create(data);
    res.json({
      success: true,
      message: "order created successfully",
      body: result,
    });
  } catch (error) {
    res.json({ success: false, message: error.message });
  }
};

export let readAllOrder = async (req, res) => {
  try {
    //   console.log(new Date(req.query.endDate).toLocaleDateString("fr-CA"))
    // console.log(new Date(req.query.endDate))
    let startdate = new Date(req.query.startDate);
    let enddate = new Date(req.query.endDate);
    let result = await Order.find({ date: { $gte: startdate, $lte: enddate } });
    res.json({
      success: true,
      message: "order read successfully",
      data: result,
    });
  } catch (error) {
    res.json({ success: false, message: error.message });
  }
};

export let readOrderById = async (req, res) => {
  let id = req.params.id;
  try {
    let result = await Order.findById(id);
    res.json({
      success: true,
      message: "order read successfully",
      data: result,
    });
  } catch (error) {
    res.json({ success: false, message: error.message });
  }
};
export let updateOrder = async (req, res) => {
  let id = req.params.id;
  let data = req.body;
  try {
    let result = await Order.findByIdAndUpdate(id, data, { new: true });
    res.json({
      success: true,
      message: "order updated successfully",
      data: result,
    });
  } catch (error) {
    res.json({ success: false, message: error.message });
  }
};
export let deleteOrder = async (req, res) => {
  let id = req.params.id;
  try {
    let result = await Order.findByIdAndDelete(id);
    res.json({
      success: true,
      message: "order deleted successfully",
      data: result,
    });
  } catch (error) {
    res.json({ success: false, message: error.message });
  }
};

export let readAllOrderByMonthAd = async (req, res) => {
  try {
    const targetYear = parseInt(req.query.year);
    const targetMonth = parseInt(req.query.month);

    const startDateOfYearMonth = new Date(targetYear, targetMonth - 1, 1);
    const endDateOfYearMonth = new Date(targetYear, targetMonth, 0);

    let query = {};

    // Check if both year and month are provided
    if (!isNaN(targetYear) && !isNaN(targetMonth)) {
      query.date = { $gte: startDateOfYearMonth, $lte: endDateOfYearMonth };
    }

    let result = await Order.find(query);

    res.json({
      success: true,
      message: "order read successfully",
      data: result,
    });
  } catch (error) {
    res.status(500).json({
      success: false,
      message: error.message,
    });
  }
};

export let readAllOrderByBs = async (req, res) => {
  try {
    const targetYear = parseInt(req.query.nepaliyear);
    const targetMonth = parseInt(req.query.nepalimonth);

    const startDateOfYearMonth = new NepaliDate(targetYear, targetMonth - 1, 1);
    const endDateOfYearMonth = new NepaliDate(targetYear, targetMonth, 0);

    let query = {};

    // Check if both year and month are provided
    if (!isNaN(targetYear) && !isNaN(targetMonth)) {
      query.date = { $gte: startDateOfYearMonth, $lte: endDateOfYearMonth };
    }

    let result = await Order.find(query);

    res.json({
      success: true,
      message: "order read successfully",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
