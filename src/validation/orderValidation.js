import Joi from "joi";
let randomValidation = Joi.object()
  .keys({
    name: Joi.string().required().min(3).max(30).messages({
      "any.required": "Name is required",
      "string.base": "Field must be string",
      "string.min": "Name must be at least 3 character",
      "string.max": "Name must be 30 caharacter ",
    }),
    email: Joi.string()
      .required()
      .custom((value, msg) => {
        let validEmail = value.match(
          /\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}\b/
        );
        if (validEmail) {
          return true;
        } else {
          return msg.message("email is not valid");
        }
      })
      .messages({
        "any.required": "Email is required",
      }),
    food: Joi.string().required().messages({
      "any.required": "Food name is required",
      "string.base": "Field must be string",
      "string.min": "Food name must be at least 3 character",
      "string.max": "Food name must be 30 caharacter ",
    }),
    
    quantity: Joi.number().required().messages({
      "any.required": "Quantity is required",
      "number.base": "field must be number",
      "number.min": "Number must be min 1 degit",
      "number.max": "Number must be 2 digit",
    }),
  })
  .unknown(false);

export default randomValidation;
