import { Router } from "express";
import {
  createOrder,
  deleteOrder,
  readAllOrder,
  readAllOrderByBs,
  readAllOrderByMonthAd,
  readOrderById,
  updateOrder,
} from "../controller/orderController.js";
import validation from "../middleware/validation.js";
import Joi from "joi";
import randomValidation from "../validation/orderValidation.js";

let orderRouter = Router();

orderRouter
  .route("/")
  .post(validation(randomValidation), createOrder)

  .get(readAllOrder);
orderRouter
  .route("/ad")
  .get(readAllOrderByMonthAd);
orderRouter
  .route("/bs")
  .get(readAllOrderByBs);

orderRouter
  .route("/:id")
  .get(readOrderById)

  .patch(updateOrder)

  .delete(deleteOrder);
export default orderRouter;
