

import cors from "cors"
import express, { json } from "express";
import connectToMongoDb from "./src/connectToDatabase/connectToMongoDb.js";
import orderRouter from "./src/routes/orderRouter.js";

let expressApp = express();
expressApp.use(json());
let port = 8000;
expressApp.use(cors());
expressApp.use("/orders", orderRouter)
expressApp.use(express.static("./public"));


expressApp.use(orderRouter)
expressApp.listen(port, () => {
  console.log(`application is connected at port ${port} successfully.`);
});


connectToMongoDb();

